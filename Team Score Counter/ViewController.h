//
//  ViewController.h
//  Team Score Counter
//
//  Created by Alex Brown on 6/28/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

//Bar Buttons

- (IBAction)doneBtn:(id)sender;

- (IBAction)submitBtn:(id)sender;


//Team 1

@property (strong, nonatomic) IBOutlet UILabel *team1Lbl;

@property (strong, nonatomic) IBOutlet UITextField *team1Txt;

@property (strong, nonatomic) IBOutlet UILabel *team1ScoreLbl;

- (IBAction)team1Stepper:(id)sender;
@property(strong, nonatomic) IBOutlet UIStepper *team1Stepper;

//Team 2

@property (strong, nonatomic) IBOutlet UILabel *team2Lbl;

@property (strong, nonatomic) IBOutlet UITextField *Team2Txt;

@property (strong, nonatomic) IBOutlet UILabel *team2ScoreLbl;

- (IBAction)team2Stepper:(id)sender;
@property (strong, nonatomic) IBOutlet UIStepper *team2Stepper;


@end

