//
//  ViewController.m
//  Team Score Counter
//
//  Created by Alex Brown on 6/28/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

UIGestureRecognizer *tapper;

- (void)viewDidLoad {
    [super viewDidLoad];

//Dismiss keyboard
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Done button reset program paramaters
- (IBAction)doneBtn:(id)sender {
    
    //Reset team title lables
    self.team1Lbl.text = @"Team 1";
    self.team2Lbl.text = @"Team 2";
    
    //Reset textfields
    self.team1Txt.text = @"";
    self.Team2Txt.text = @"";
    
    //Replace textfields placeholder
    self.team1Txt.placeholder = @"-Enter Text-";
    self.Team2Txt.placeholder = @"-Enter Text-";
    
    //Reset stepper and label score
    self.team1ScoreLbl.text = @"0";
    self.team1Stepper.value = 0;
    
    self.team2ScoreLbl.text = @"0";
    self.team2Stepper.value = 0;
    
}

//Submit team names
- (IBAction)submitBtn:(id)sender {
    self.team1Lbl.text = self.team1Txt.text;
    self.team2Lbl.text = self.Team2Txt.text;
}

//Team 1 stepper
- (IBAction)team1Stepper:(id)sender {
    UIStepper* stepper = (UIStepper*)sender;
    
    self.team1ScoreLbl.text = [NSString stringWithFormat:@"%.f", stepper.value];
    
    //Alert view
    if ((stepper.value >= 21)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.team1Lbl.text
                                                        message:@"You won!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

//Team 2 stepper
- (IBAction)team2Stepper:(id)sender {
    UIStepper* stepper = (UIStepper*)sender;
    
    self.team2ScoreLbl.text = [NSString stringWithFormat:@"%.f", stepper.value];
    
    //Alert view
    if ((stepper.value >= 21)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.team2Lbl.text
                                                        message:@"You won!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

//Dismiss keyboard
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

@end
